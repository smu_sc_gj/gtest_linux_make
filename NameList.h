#ifndef NAMELIST_H
#define NAMELIST_H

#include <vector>
#include <string>
#include <random>

using std::string;
using std::vector;

class NameList
{
private:
    vector<string> nameList;
    std::mt19937 rng;

public:
    NameList();
    ~NameList();

    void addName(string name);
    int getSize();

    void loadFile(string fileName);

    typedef std::vector<string>::iterator iterator;
    typedef std::vector<string>::const_iterator const_iterator;

    iterator begin() { return nameList.begin(); }

    const_iterator begin() const { return nameList.begin(); }

    iterator end() { return nameList.end(); }

    const_iterator end() const { return nameList.end(); }

     NameList::iterator pickName(string&);
};

#endif // NAMELIST_H
